import { Loan } from './loan';

export class LoanDetailed extends Loan {
  bookTitle: string;
  userName: string;
}
