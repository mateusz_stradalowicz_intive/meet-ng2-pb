import { FormsModule } from '@angular/forms';
import { LoanService } from './loan.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoanListComponent } from './loan-list/loan-list.component';
import { AddLoanComponent } from './add-loan/add-loan.component';

export * from './loan-list/loan-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    LoanListComponent,
    AddLoanComponent
  ],
  exports: [
    LoanListComponent
  ],
  providers: [
    LoanService
  ]
})
export class LoanModule { }
