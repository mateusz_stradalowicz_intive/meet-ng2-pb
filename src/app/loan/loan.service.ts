import { Book } from './../book/book';
import { User } from './../user/user';
import { Http } from '@angular/http';
import { LoanDetailed } from './loan-detailed';
import { Loan } from './loan';
import { Injectable } from '@angular/core';

import * as Enumerable from 'linq';

@Injectable()
export class LoanService {

  constructor(private http: Http) { }

  getLoansDetailed(): Promise<LoanDetailed[]> {
    const usersPromise: Promise<User[]> = this.http.get('api/users')
      .toPromise()
      .then(response => response.json().data);

    const booksPromise: Promise<Book[]> = this.http.get('api/books')
      .toPromise()
      .then(response => response.json().data);

    const loansPromise: Promise<Loan[]> = this.http.get('api/loans')
      .toPromise()
      .then(response => response.json().data);

    const result: Promise<LoanDetailed[]> = Promise.all([usersPromise, booksPromise, loansPromise]).then((promisesResults) => {
      let loansDetailed =
        Enumerable.from(promisesResults[2]).join(promisesResults[0], loan => loan.userId, users => users.id, (loan, user) => {
          const loanDetailed = new LoanDetailed();
          loanDetailed.id = loan.id;
          loanDetailed.userId = loan.userId;
          loanDetailed.bookId = loan.bookId;
          loanDetailed.userName = user.name;
          return loanDetailed;
        });

      loansDetailed = loansDetailed.join(promisesResults[1], loansDet => loansDet.bookId, book => book.id, (loansDet, book) => {
        loansDet.bookTitle = book.title;
        return loansDet;
      });
      return loansDetailed.toArray();
    });
    return result;
  }

  delete(loanDetailed: LoanDetailed): Promise<User> {
    return this.http.delete(`api/loans/${loanDetailed.id}`)
      .toPromise()
      .then(() => loanDetailed = undefined);
  }
}
