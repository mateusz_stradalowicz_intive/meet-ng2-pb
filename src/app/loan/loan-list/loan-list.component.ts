import { LoanDetailed } from './../loan-detailed';
import { LoanService } from './../loan.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loan-list',
  templateUrl: './loan-list.component.html',
  styleUrls: ['./loan-list.component.css']
})
export class LoanListComponent implements OnInit {
  loansDetailed: LoanDetailed[];

  constructor(private loansService: LoanService) { }

  ngOnInit() {
    this.loadLoansDetailed();
  }

  deleteLoan(loanDetailed: LoanDetailed) {
    this.loansService.delete(loanDetailed);
    this.loadLoansDetailed();
  }

  loadLoansDetailed() {
    this.loansDetailed = null;
    this.loansService.getLoansDetailed().then(result => this.loansDetailed = result);
  }
}
