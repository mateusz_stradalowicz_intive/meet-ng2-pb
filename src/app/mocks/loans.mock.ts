import { Loan } from './../loan/loan';

export const LOANS: Loan[] = [
  { id: 0, userId: 1, bookId: 2 },
  { id: 1, userId: 2, bookId: 4 },
  { id: 2, userId: 3, bookId: 3 },
];
