import { InMemoryDbService } from 'angular-in-memory-web-api';

import { USERS } from './user.mock';
import { BOOKS } from './book.mock';
import { LOANS } from './loans.mock';

export class InMemoryService implements InMemoryDbService {
  createDb() {
    const users = USERS;
    const books = BOOKS;
    const loans = LOANS;
    return { users, books, loans };
  }
}
