import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/toPromise';

import * as Enumerable from 'linq';

import { User } from './user';

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  getUsers(): Promise<User[]> {
    return this.http.get('api/users')
      .toPromise()
      .then(response => response.json().data);
  }

  create(login: string, name: string): Promise<User> {
    return this.getUsers().then(users => {
      const enumerableUsers = Enumerable.from(users);
      const id = enumerableUsers.any() ? enumerableUsers.max(item => item.id) + 1 : 0;
      const result: Promise<User> = this.http
        .post('api/users', JSON.stringify({ id, login, name }))
        .toPromise()
        .then(res => res.json().data);
      return result;
    });
  }

  delete(user: User): Promise<User> {
    return this.http.delete(`api/users/${user.id}`)
      .toPromise()
      .then(() => user = undefined);
  }
}
